Exemplo de banco de dados com varias tabelas e registros.


# Tabelas

* actor
* address
* category
* city
* country
* customer
* customer_list
* film
* film_actor
* film_category
* film_list
* film_text
* inventory
* language
* nicer_but_slower_film_list
* payment
* rental
* sales_by_film_category
* sales_by_store
* staff
* staff_list
* store

# Informações

* 23 tabelas
* 50.086 registros
* 6.6MB
* utf8_general_ci